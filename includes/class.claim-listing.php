<?php

class VenueTown_Claim_Listings {

    /**
     * Constructor
     */
    public function __construct() {
        add_action('admin_menu', array($this, 'vtcl_menu'));
        add_action('admin_enqueue_scripts', array($this, 'vtcl_admin_assets'));
        add_action('init', array($this, 'vtcl_start_session'));
    }

    /**
     * On wordpress init start session
     */
    public function vtcl_start_session() {
        session_start();
    }

    /**
     * Register assets for wordpress admin
     */
    public function vtcl_admin_assets() {
        wp_register_style('vtcl_css', plugin_dir_url(__FILE__) . '../assets/style.css', false, rand());
        wp_enqueue_style('vtcl_css');
    }

    /**
     * VenueTown Claim Listings Plugin Menu
     */
    public function vtcl_menu() {
        add_menu_page("VenueTown Claim Listings", "VenueTown Claim Listings", 'administrator', 'vtcl', array($this, 'vtcl_page'));
    }

    /**
     * Add error, success and update notices
     * @param string $notice
     * @param string $type
     */
    public function vtcl_add_notices($notice, $type = "error") {
        $data = array(
            'message' => $notice,
            'type' => $type
        );
        $_SESSION['vtcl_notice'] = $data;
    }

    /**
     * Show error, success and update notice
     */
    public function vtcl_show_notice() {
        if ($_SESSION['vtcl_notice']) {
            $data = $_SESSION['vtcl_notice'];
            ?>
            <div class="notice notice-<?php echo $data['type']; ?> is-dismissible">
                <p><?php echo $data['message']; ?></p>
            </div>
            <?php
            unset($_SESSION['vtcl_notice']);
        }
    }

    /**
     * Claim lising content page
     */
    public function vtcl_page() {
        $title = "VenueTown Claim Listings";
        $error = null;

        if (isset($_POST['vtcl_proceed']) && isset($_POST['vtcl_post_id']) && isset($_POST['vtcl_user_id'])) {
            $post_id = $_POST['vtcl_post_id'];
            $user_id = $_POST['vtcl_user_id'];

            if ($post = get_post($post_id)) {
                if ($user = get_userdata($user_id)) {
                    $user_info = get_userdata($post->post_author);
                    include('templates/post-detail.php');
                } else {
                    $this->vtcl_add_notices('Invalid user id');
                    wp_redirect('?page=vtcl');
                }
            } else {
                $this->vtcl_add_notices('Invalid post id');
                wp_redirect('?page=vtcl');
            }
        }

        if (isset($_POST['vtcl_claim']) && isset($_POST['vtcl_post_id']) && isset($_POST['vtcl_user_id'])) {
            if ($this->vtcl_transfer_post($_POST['vtcl_post_id'], $_POST['vtcl_user_id'], $_POST['vtcl_post_type'])) {
                $this->vtcl_add_notices('Successfully Claimed', 'success');
                wp_redirect('?page=vtcl');
            }
        }

        if (!$_POST) {
            include('templates/claim-listings.php');
        }
    }

    /**
     * Transfer post
     * @global type $wpdb
     * @param type $post_id
     * @param type $user_id
     * @param type $post_type
     * @return boolean
     */
    public function vtcl_transfer_post($post_id, $user_id, $post_type) {
        global $wpdb;
        $wpdb->update($wpdb->prefix . 'posts', array('post_author' => $user_id), array('ID' => $post_id));

        if ($attach_id = get_post_thumbnail_id($post_id)) {
            $this->vtcl_move_media($attach_id, $post_type, $user_id);
        }
        
        $this->vtcl_trasnfer_meta_assets($post_id, $post_type, $user_id, 'image_gallery');
        $this->vtcl_trasnfer_meta_assets($post_id, $post_type, $user_id, 'attachment_pictures');
        $this->vtcl_trasnfer_meta_assets($post_id, $post_type, $user_id, 'packages_attachment');
        $this->vtcl_trasnfer_meta_assets($post_id, $post_type, $user_id, 'docs_attachment');

        return true;
    }
    
    /**
     * Trasnfer post meta fields assets like (gallery, attachments, floor plan, etc)
     * @param integer $post_id
     * @param string $post_type
     * @param integer $user_id
     * @param string $meta_key
     * @return boolean
     */
    public function vtcl_trasnfer_meta_assets($post_id, $post_type, $user_id, $meta_key) {
        if ($assets = get_post_meta($post_id, $meta_key)) {
            foreach ($assets as $asset) {
                foreach ($asset as $attach_id => $url) {
                    $asset_data[$attach_id] = $this->vtcl_move_media($attach_id, $post_type, $user_id);
                }
            }
            update_post_meta($post_id, $meta_key, $asset_data);
            
            return true;
        }
    }

    /**
     * 
     * @param type $attach_id
     * @param type $post_type
     * @param type $user_id
     * @return string
     */
    public function vtcl_move_media($attach_id, $post_type, $user_id) {

        $media_path = get_attached_file($attach_id);
        $user_info = get_userdata($user_id);
        $upload_directory = wp_upload_dir(date('Y/m'));
        $file = basename($media_path);
        $filename = $upload_directory['path'] . '/' . $user_info->user_login . '/' . $post_type . '/' . $file;
        $file_user_base = $upload_directory['path'] . '/' . $user_info->user_login . '/' . $post_type . '/';

        if (file_exists($media_path)) {
            if (copy($media_path, $filename)) {

                $attachment_metadata = wp_get_attachment_metadata($attach_id);

                foreach ($attachment_metadata['sizes'] as $size_name => $size_data) {
                    if ($size_data['file']) {
                        $size_path = pathinfo($media_path);

                        if (file_exists($size_path['dirname'] . "/" . $size_data['file'])) {
                            copy($size_path['dirname'] . "/" . $size_data['file'], $file_user_base . $size_data['file']);
                        }
                    }
                }

                update_attached_file($attach_id, $filename);
                return $upload_directory['url'] . "/" . $attachment_metadata['file'];
            }
        }
    }

}
