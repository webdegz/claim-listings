<div class="wrap">
    <h1><?php echo $title; ?></h1>
    <hr>
    
    <?php $this->vtcl_show_notice(); ?>
    
    <form method="post">        
        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row"><label for="vtcl_post_id">Post ID:</label></th>
                    <td>
                        <input type="text" name="vtcl_post_id" id="vtcl_post_id" required="required">
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="vtcl_user_id">User ID:</label></th>
                    <td>
                        <input type="text" name="vtcl_user_id" id="vtcl_user_id" required="required">
                    </td>
                </tr>
            </tbody>
        </table>
        <p class="submit"><input type="submit" name="vtcl_proceed" id="vtcl_proceed" required="required" class="button button-primary" value="Proceed"></p>        
    </form>
</div>