<div class="wrap">
    <h1><?php echo $title; ?></h1>
    <hr>
    
    <h3>Post Detail</h3>
    <table class="wp-list-table widefat fixed striped">
        <thead>
            <tr>
                <th scope="col" id="author" class="manage-column column-author">Label</th>
                <th scope="col" id="title" class="manage-column column-title column-primary"><span>Value</span></th>
            </tr>
        </thead>
        <tbody id="the-list">
            <tr class="author-other level-0 post-674 type-venue status-publish has-post-thumbnail hentry venue_category-abc venue_category-def venue_language-other venue_tags_specializations-sky">
                <td class="author column-author" data-colname="Author"><b>Post Type</b></td>
                <td class="title column-title has-row-actions column-primary page-title" data-colname="Title"><?php echo ucfirst($post->post_type); ?></td>
            </tr>
            
            <tr class="author-other level-0 post-674 type-venue status-publish has-post-thumbnail hentry venue_category-abc venue_category-def venue_language-other venue_tags_specializations-sky">
                <td class="author column-author" data-colname="Author"><b>Post ID</b></td>
                <td class="title column-title has-row-actions column-primary page-title" data-colname="Title"><?php echo $post->ID; ?></td>
            </tr>
            
            <tr class="author-other level-0 post-674 type-venue status-publish has-post-thumbnail hentry venue_category-abc venue_category-def venue_language-other venue_tags_specializations-sky">
                <td class="author column-author" data-colname="Author"><b>Post Title</b></td>
                <td class="title column-title has-row-actions column-primary page-title" data-colname="Title"><?php echo $post->post_title; ?></td>
            </tr>
            
            <tr class="author-other level-0 post-674 type-venue status-publish has-post-thumbnail hentry venue_category-abc venue_category-def venue_language-other venue_tags_specializations-sky">
                <td class="author column-author" data-colname="Author"><b>User ID</b></td>
                <td class="title column-title has-row-actions column-primary page-title" data-colname="Title"><?php echo $post->post_author; ?></td>
            </tr>
            
            <tr class="author-other level-0 post-674 type-venue status-publish has-post-thumbnail hentry venue_category-abc venue_category-def venue_language-other venue_tags_specializations-sky">
                <td class="author column-author" data-colname="Author"><b>Username</b></td>
                <td class="title column-title has-row-actions column-primary page-title" data-colname="Title"><?php echo $user_info->user_login; ?></td>
            </tr>
            
            <tr class="author-other level-0 post-674 type-venue status-publish has-post-thumbnail hentry venue_category-abc venue_category-def venue_language-other venue_tags_specializations-sky">
                <td class="author column-author" data-colname="Author"><b>Post Date</b></td>
                <td class="title column-title has-row-actions column-primary page-title" data-colname="Title"><?php echo $post->post_date_gmt; ?></td>
            </tr>
        </tbody>        
    </table>    
    <form method="post">
        <input type="hidden" name="vtcl_post_id" value="<?php echo $_POST['vtcl_post_id']; ?>">
        <input type="hidden" name="vtcl_user_id" value="<?php echo $_POST['vtcl_user_id']; ?>">
        <input type="hidden" name="vtcl_post_type" value="<?php echo $post->post_type; ?>">
        <p class="submit"><input type="submit" name="vtcl_claim" id="vtcl_claim" class="button button-primary" value="Claim"></p>      
    </form>
</div>