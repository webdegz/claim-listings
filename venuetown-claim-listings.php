<?php
/*
  Plugin Name: VenueTown Claim Listings
  Plugin URI: http://codup.io/
  Description: Change lisitng owners
  Version: 1.1.1.1
  Author Name: Codup
  Author URI: http://codup.io/
 */
if (!defined('DS')) {
    define("DS", DIRECTORY_SEPARATOR);
}
if (!defined('VERSION')) {
    define("VERSION", '1.1.1.0');
}

include(dirname(__FILE__) . DS . 'includes' . DS . 'class.claim-listing.php');
new VenueTown_Claim_Listings();